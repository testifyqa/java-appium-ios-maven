
Feature: BaseScenarios
  These scenarios can be used in any project

  Scenario: 01. Validate the PageSource string on the app screen
    Then the user sees "Vincent Willem van Gogh" in the PageSource

  Scenario: 02. Validate existence of multiple texts in PageSource
    Then the user sees
      | Pablo Picasso    |
      | Spanish painter  |
      | sculptor         |
      | printmaker       |

  Scenario: 03. Validate the Current Context of the App
    Then the user sees the current context is "NATIVE_APP"